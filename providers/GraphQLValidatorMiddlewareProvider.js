const { ServiceProvider } = require.main.require('@adonisjs/fold')
const validator = require('../src')

class GraphQLValidatorProvider extends ServiceProvider {
  register () {
    this.app.bind('Adonis/GraphQL/Middleware/Validator', () => validator)
  }
}

module.exports = GraphQLValidatorProvider
