const { argsToObject } = require('./helpers')

const validator = (rules) => (next) => async (root, args, context, info) => {
  const { validateAll } = use('Validator')
  args = argsToObject(args)
  const validation = await validateAll(args, rules)

  if (validation.fails()) {
    const errs = validation.messages().reduce((byField, item) => {
      byField[item.field] = byField[item.field] || []
      byField[item.field] = byField[item.field].concat([item.validation])
      return byField
    }, {})

    const err = new Error('Validation Error')
    err.extensions = { validation: errs }
    throw err
  }
  return next(root, args, context, info)
}

module.exports = validator
