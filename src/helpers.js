const argsToObject = (args) => {
  return Object.keys(args).reduce((obj, key) => {
    if (!obj[key].constructor) {
      obj[key] = Object.assign({}, argsToObject(obj[key]))
    }
    return obj
  }, args)
}

module.exports = { argsToObject }
